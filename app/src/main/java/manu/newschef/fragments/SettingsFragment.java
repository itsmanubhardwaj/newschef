package manu.newschef.fragments;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import com.nex3z.togglebuttongroup.MultiSelectToggleGroup;

import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import manu.newschef.R;
import manu.newschef.application.MyApplication;
import manu.newschef.utils.NewsBroadcastReceiver;
import manu.newschef.utils.SharedPrefs;
import manu.newschef.utils.TextToSpeechFeature;

/**
 * Created by imman on 3/24/2018.
 */

public class SettingsFragment extends Fragment {

    @BindView(R.id.service_toggle)
    ToggleButton serviceToggle;
    @BindView(R.id.service_settings)
    LinearLayout advanceSettingsLayout;
    @BindView(R.id.category_group)
    MultiSelectToggleGroup category;
    @BindView(R.id.countries_group)
    MultiSelectToggleGroup country;
    private SettingsFragmentListener listener;
    private Set<String> checkedCountryIds = new HashSet<>();
    private Set<String> checkedCategoryIds = new HashSet<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, view);
        if (listener != null) {
            listener.setFragment(this);
            listener.setToolbarTitle("Settings");
        }
        serviceToggle.setChecked(SharedPrefs.getButton());
        if (SharedPrefs.getButton()) {
            advanceSettingsLayout.setVisibility(View.VISIBLE);
        } else {
            advanceSettingsLayout.setVisibility(View.GONE);
        }
        if (SharedPrefs.getCheckedCountryIds() != null) {
            checkedCountryIds = SharedPrefs.getCheckedCountryIds();
            for (String s : checkedCountryIds) {
                country.check(Integer.parseInt(s));
            }
        }
        if (SharedPrefs.getCheckedCategoryIds() != null) {
            checkedCategoryIds = SharedPrefs.getCheckedCategoryIds();
            for (String str : checkedCategoryIds) {
                category.check(Integer.parseInt(str));
            }
        }
        serviceToggle.setOnClickListener(v -> {
            Intent newsIntent = new Intent(MyApplication.getAppContext(), NewsBroadcastReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(MyApplication.getAppContext(),
                    0, newsIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            if (serviceToggle.isChecked()) {
                SharedPrefs.setButton(true);
                long time = System.currentTimeMillis() + 5000;
                MyApplication.getAlarmManager().setRepeating(AlarmManager.RTC_WAKEUP, time, 12000, pendingIntent);
                TextToSpeechFeature.speakText("Hey, happy to have you onboard", 0);
                advanceSettingsLayout.setVisibility(View.VISIBLE);
            } else {
                SharedPrefs.setButton(false);
                MyApplication.getAlarmManager().cancel(pendingIntent);
                TextToSpeechFeature.speakText("It's hard to say goodbye", 0);
                advanceSettingsLayout.setVisibility(View.GONE);
            }
        });
        country.setOnCheckedChangeListener((group, checkedId, isChecked) -> {
            Set<Integer> ids = group.getCheckedIds();
            checkedCountryIds = new HashSet<>(ids.size());
            for (Integer integer : ids)
                checkedCountryIds.add(integer.toString());
            SharedPrefs.setCheckedCountryIds(checkedCountryIds);
        });
        category.setOnCheckedChangeListener((group, checkedId, isChecked) -> {
            Set<Integer> ids = group.getCheckedIds();
            checkedCategoryIds = new HashSet<>(ids.size());
            for (Integer integer : ids)
                checkedCategoryIds.add(integer.toString());
            SharedPrefs.setCheckedCategoryIds(checkedCategoryIds);
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof SettingsFragmentListener) {
            listener = (SettingsFragmentListener) context;
        } else {
            throw new RuntimeException("Please implement SettingsFragmentListener interface in your activity " +
                    "if you want to use Settings fragment");
        }
    }

    public interface SettingsFragmentListener {
        void setToolbarTitle(String title);

        void setFragment(Fragment fragment);
    }
}

