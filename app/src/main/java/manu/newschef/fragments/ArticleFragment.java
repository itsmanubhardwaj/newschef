package manu.newschef.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import manu.newschef.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by imman on 11/5/2017.
 */

public class ArticleFragment extends Fragment {

    private static String TITLE_KEY = "title";
    @BindView(R.id.news_web_view)
    WebView newsView;
    private String articleUrl;
    private static String URL_KEY = "urlKey";
    private ArticleFragmentListener listener;
    private String title;

    public static ArticleFragment createInstance(String urlKey, String title) {
        ArticleFragment fragment = new ArticleFragment();
        Bundle arg = new Bundle();
        arg.putString(URL_KEY, urlKey);
        arg.putString(TITLE_KEY, title);
        fragment.setArguments(arg);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        articleUrl = getArguments().getString(URL_KEY);
        title = getArguments().getString(TITLE_KEY);
        if (savedInstanceState != null)
            title = savedInstanceState.getString(TITLE_KEY);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article, container, false);
        ButterKnife.bind(this, view);
        if (listener != null) {
            listener.setFragment(this);
            listener.setToolbarTitle(title);
        }
        newsView.getSettings().setJavaScriptEnabled(true);
        ProgressDialog dialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        dialog.setMessage("Loading");
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        if (savedInstanceState == null) {
            newsView.loadUrl(articleUrl);
        } else {
            newsView.restoreState(savedInstanceState);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            newsView.setWebViewClient(new WebViewClient() {

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        view.loadUrl(request.getUrl().toString());
                    }
                    return true;
                }
            });
        } else if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            newsView.setWebViewClient(new WebViewClient() {

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }
            });
        }
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        newsView.saveState(outState);
        outState.putString(TITLE_KEY, title);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof ArticleFragmentListener) {
            listener = (ArticleFragmentListener) context;
        } else {
            throw new RuntimeException("Please implement ArticleFragmentListener interface in your activity " +
                    "if you want to use Article fragment");
        }
    }

    public interface ArticleFragmentListener {
        void setFragment(Fragment fragment);

        void setToolbarTitle(String title);
    }
}
