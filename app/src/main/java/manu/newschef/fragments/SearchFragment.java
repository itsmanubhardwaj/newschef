package manu.newschef.fragments;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import manu.newschef.R;
import manu.newschef.activities.MainActivity;
import manu.newschef.application.MyApplication;
import manu.newschef.models.Article;
import manu.newschef.models.NewsAdapter;
import manu.newschef.response.SearchResponse;
import manu.newschef.services.NewsServiceBuilder;
import manu.newschef.utils.RxUtils;
import manu.newschef.utils.TextToSpeechFeature;
import manu.newschef.views.NewsViewHolder;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by imman on 2/19/2018.
 */

public class SearchFragment extends Fragment implements NewsViewHolder.NewsClickListener {

    View view;
    private List<Article> newsResponse = new ArrayList<>();
    private RecyclerView searchRecyclerView;
    private ProgressBar progressBar;
    private TextView message;
    public ObservableField<String> searchKey = new ObservableField<String>();
    private SearchFragmentListener listener;
    private NewsAdapter adapter;
    private Gson gson = new Gson();
    private Type type = new TypeToken<List<Article>>() {
    }.getType();
    private static String RESPONSE_KEY = "response";
    private final int REQ_CODE_SPEECH_INPUT = 100;
    private ImageView voice;
    private EditText searchField;
//    private NewsDatabase newsDatabase = new NewsDatabase();
//    private int i = 0;
//    private NotificationCompat.Builder builder;
//    private NotificationManager manager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            String json = savedInstanceState.getString(RESPONSE_KEY);
            newsResponse = gson.fromJson(json, type);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);
        ((MainActivity) getActivity()).setToolbarTitle("Search");
        if (listener != null) {
            listener.setFragment(this);
            listener.setToolbarTitle("Search");
        }
        progressBar = view.findViewById(R.id.progressBar);
        voice = view.findViewById(R.id.voice);
        message = view.findViewById(R.id.message_text_view);
        searchRecyclerView = view.findViewById(R.id.search_recycler_view);
        searchRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        voice.setOnClickListener(view -> {
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                    RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                    "Say something");
            try {
                startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
            } catch (ActivityNotFoundException a) {
                Toast.makeText(MyApplication.getAppContext(),
                        "Not supported",
                        Toast.LENGTH_SHORT).show();
            }
        });

        if (savedInstanceState != null) {
            progressBar.setVisibility(View.GONE);
            searchRecyclerView.setVisibility(View.VISIBLE);
            adapter = new NewsAdapter(newsResponse, SearchFragment.this);
            searchRecyclerView.setAdapter(adapter);
        }

//        builder = new NotificationCompat.Builder(getActivity());
//        manager = (NotificationManager) MyApplication.getAppContext().getSystemService(NOTIFICATION_SERVICE);

        searchField = view.findViewById(R.id.search_field);
        searchField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String s = editable.toString();
                searchKey.set(s);
            }
        });

        RxUtils.toObservable(searchKey).filter(value -> value.length() >= 3)
                .debounce(1, TimeUnit.SECONDS)
                .distinctUntilChanged()
                .subscribe(this::callApi);

        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                if (direction == ItemTouchHelper.RIGHT) {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_TEXT, adapter.list.get(position).getUrl());
                    startActivity(shareIntent);
                    adapter.notifyDataSetChanged();
                }
                if (direction == ItemTouchHelper.LEFT) {
                    TextToSpeechFeature.speakText(newsResponse.get(position).getTitle(), 0);
                    TextToSpeechFeature.speakText(newsResponse.get(position).getDescription(), 1);
//                    newsDatabase.addArticle(newsResponse.get(position));
//                    builder.setContentTitle("News Saved");
//                    builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.news));
//                    builder.setSmallIcon(R.drawable.save);
//                    builder.setContentText("The news article has been saved.");
//                    builder.setAutoCancel(true);
//                    Notification notification = builder.build();
//                    manager.notify(i++, notification);
                    adapter.notifyDataSetChanged();
                }
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(searchRecyclerView);
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    callApi(result.get(0));
                    searchField.setText(result.get(0));
                }
                break;
            }
        }
    }

    public void callApi(String text) {
        this.getActivity().runOnUiThread(() -> {
            progressBar.setVisibility(View.VISIBLE);
            searchRecyclerView.setVisibility(View.GONE);
        });
        NewsServiceBuilder.provideNewsService()
                .getSearchResults(text, "popularity").
                enqueue(new Callback<SearchResponse>() {
                            @Override
                            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                                progressBar.setVisibility(View.GONE);
                                if (response.body().getArticles().size() <= 0) {
                                    message.setVisibility(View.VISIBLE);
                                    message.setText("No result found. Try again with different keyword");
                                } else {
                                    searchRecyclerView.setVisibility(View.VISIBLE);
                                    newsResponse = response.body().getArticles();
                                    adapter = new NewsAdapter(newsResponse, SearchFragment.this);
                                    searchRecyclerView.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                }
                            }

                            @Override
                            public void onFailure(Call<SearchResponse> call, Throwable t) {
                                Snackbar.make(view, t.getMessage(), Snackbar.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                                message.setVisibility(View.VISIBLE);
                                message.setText("No result found. Try again with different a keyword/phrase.");
                            }
                        }
                );
    }

    @Override
    public void onPause() {
        super.onPause();
        TextToSpeechFeature.stopSpeech();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        TextToSpeechFeature.stopSpeech();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        String json = gson.toJson(newsResponse, type);
        outState.putString(RESPONSE_KEY, json);
    }

    @Override
    public void onNewsItemClicked(String url) {
        if (listener != null)
            listener.onNewsItemClick(url);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof SearchFragmentListener) {
            listener = (SearchFragmentListener) context;
        } else {
            throw new RuntimeException("Please implement SearchFragmentListener interface in your activity " +
                    "if you want to use Search fragment");
        }
    }

    public interface SearchFragmentListener {
        void setFragment(Fragment fragment);

        void setToolbarTitle(String title);

        void onNewsItemClick(String url);
    }
}
