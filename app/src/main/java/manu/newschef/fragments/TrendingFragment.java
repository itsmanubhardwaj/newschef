package manu.newschef.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import manu.newschef.R;
import manu.newschef.models.ApiError;
import manu.newschef.models.Article;
import manu.newschef.models.NewsAdapter;
import manu.newschef.models.NewsCallback;
import manu.newschef.response.News;
import manu.newschef.services.NewsServiceBuilder;
import manu.newschef.utils.TextToSpeechFeature;
import manu.newschef.views.NewsViewHolder;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by imman on 11/5/2017.
 */

public class TrendingFragment extends Fragment implements NewsViewHolder.NewsClickListener {

    private List<Article> newsResponse = new ArrayList<>();
    @BindView(R.id.swipe_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.headlines_recycler_view)
    RecyclerView headlines_recycler_view;
    private NewsAdapter adapter;
    private ProgressDialog dialog;
    private Gson gson = new Gson();
    private Type type = new TypeToken<List<Article>>() {
    }.getType();
    private TrendingFragmentListener listener;
    private static String RESPONSE_KEY = "response";
    private ImageView retryImage;
    private TextView retryText;
//    private int i = 0;
//    private NotificationCompat.Builder builder;
//    private NotificationManager manager;
//    private NewsDatabase newsDatabase = new NewsDatabase();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            String json = savedInstanceState.getString(RESPONSE_KEY);
            newsResponse = gson.fromJson(json, type);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler_view, container, false);
        ButterKnife.bind(this, view);
        headlines_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (listener != null) {
            listener.setFragment(this);
            listener.setToolbarTitle("Trending");
        }
        retryImage = view.findViewById(R.id.retry_image);
        retryText = view.findViewById(R.id.retry_button);
        retryText.setOnClickListener(view1 -> callApi());
        dialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        dialog.setMessage("Loading");
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);

        if (savedInstanceState == null) {
            dialog.show();
            callApi();
        } else {
            adapter = new NewsAdapter(newsResponse, TrendingFragment.this);
            headlines_recycler_view.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
//        builder = new NotificationCompat.Builder(getActivity());
//        manager = (NotificationManager) MyApplication.getAppContext().getSystemService(NOTIFICATION_SERVICE);

        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this::callApi);


        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                if (direction == ItemTouchHelper.RIGHT) {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_TEXT, adapter.list.get(position).getUrl());
                    startActivity(shareIntent);
                    adapter.notifyDataSetChanged();
                }
                if (direction == ItemTouchHelper.LEFT) {
                    TextToSpeechFeature.speakText(newsResponse.get(position).getTitle(), 0);
                    TextToSpeechFeature.speakText(newsResponse.get(position).getDescription(), 1);
//                    newsDatabase.addArticle(newsResponse.get(position));
//                    builder.setContentTitle("News Saved");
//                    builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.news));
//                    builder.setSmallIcon(R.drawable.save);
//                    builder.setContentText("The news article has been saved.");
//                    builder.setAutoCancel(true);
//                    Notification notification = builder.build();
//                    manager.notify(i++, notification);
                    adapter.notifyDataSetChanged();
                }
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(headlines_recycler_view);
        return view;
    }

    private void callApi() {
        NewsServiceBuilder.provideNewsService().getNews("google-news").enqueue(new NewsCallback<News>() {
            @Override
            public void onSuccess(News response) {
                if (dialog.isShowing())
                    dialog.cancel();
                newsResponse = response.getArticles();
                if(newsResponse.size()<=0)
                {
                    swipeRefreshLayout.setVisibility(View.GONE);
                    retryImage.setVisibility(View.VISIBLE);
                    retryText.setVisibility(View.VISIBLE);
                } else {
                    adapter = new NewsAdapter(newsResponse, TrendingFragment.this);
                    headlines_recycler_view.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onError(ApiError error) {
                swipeRefreshLayout.setVisibility(View.GONE);
                retryImage.setVisibility(View.VISIBLE);
                retryText.setVisibility(View.VISIBLE);
                dialog.cancel();
                Snackbar.make(swipeRefreshLayout, error.message, Snackbar.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        String json = gson.toJson(newsResponse, type);
        outState.putString(RESPONSE_KEY, json);
    }

    @Override
    public void onPause() {
        super.onPause();
        TextToSpeechFeature.stopSpeech();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        TextToSpeechFeature.stopSpeech();
    }

    @Override
    public void onNewsItemClicked(String url) {
        if (listener != null)
            listener.onNewsItemClick(url);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof TrendingFragmentListener) {
            listener = (TrendingFragmentListener) context;
        } else {
            throw new RuntimeException("Please implement TrendingFragmentListener interface in your activity " +
                    "if you want to use Trending fragment");
        }
    }

    public interface TrendingFragmentListener {
        void setToolbarTitle(String title);

        void setFragment(Fragment fragment);

        void onNewsItemClick(String url);
    }
}
