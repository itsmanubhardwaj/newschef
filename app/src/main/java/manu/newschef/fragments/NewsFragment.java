package manu.newschef.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import manu.newschef.R;
import manu.newschef.models.ApiError;
import manu.newschef.models.Article;
import manu.newschef.models.NewsAdapter;
import manu.newschef.models.Source;
import manu.newschef.models.NewsCallback;
import manu.newschef.response.News;
import manu.newschef.services.NewsDatabase;
import manu.newschef.services.NewsServiceBuilder;
import manu.newschef.utils.TextToSpeechFeature;
import manu.newschef.views.NewsViewHolder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsFragment extends Fragment implements NewsViewHolder.NewsClickListener {

    private static String SOURCE_KEY = "sourceKey";
    private static String RESPONSE_KEY = "response";
    private static String TITLE_KEY = "title";
    private List<Article> newsResponse = new ArrayList<>();
    @BindView(R.id.swipe_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.headlines_recycler_view)
    RecyclerView headlines_recycler_view;
    private ProgressDialog dialog;
    private NewsFragmentListener listener;
    private TextView retryText;
    private ImageView retryImage;
    private String sourceKey;
    private NewsAdapter adapter;
    private Gson gson = new Gson();
    private Type type = new TypeToken<List<Article>>() {
    }.getType();
    private String title;
    private NewsDatabase newsDatabase = new NewsDatabase();
//    private int i = 0;
//    private NotificationCompat.Builder builder;
//    private NotificationManager manager;

    public static NewsFragment createInstance(Source source) {
        NewsFragment fragment = new NewsFragment();
        Bundle arg = new Bundle();
        arg.putString(SOURCE_KEY, source.sourceKey);
        arg.putString(TITLE_KEY, source.sourceName);
        fragment.setArguments(arg);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sourceKey = getArguments().getString(SOURCE_KEY);
        title = getArguments().getString(TITLE_KEY);
        if (savedInstanceState != null) {
            String json = savedInstanceState.getString(RESPONSE_KEY);
            newsResponse = gson.fromJson(json, type);
            title = savedInstanceState.getString(TITLE_KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler_view, container, false);
        ButterKnife.bind(this, view);
        if(listener!=null) {
            listener.setFragment(this);
            listener.setToolbarTitle(title);
        }
        headlines_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
        retryImage = view.findViewById(R.id.retry_image);
        retryText = view.findViewById(R.id.retry_button);
        retryText.setOnClickListener(view1 -> callApi());
        dialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        dialog.setMessage("Loading");
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);

        if (savedInstanceState == null) {
            dialog.show();
            callApi();
        } else {
            adapter = new NewsAdapter(newsResponse, NewsFragment.this);
            headlines_recycler_view.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this::callApi);

//        builder = new NotificationCompat.Builder(getActivity());
//        manager = (NotificationManager) MyApplication.getAppContext().getSystemService(NOTIFICATION_SERVICE);

        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                if (direction == ItemTouchHelper.RIGHT) {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_TEXT, adapter.list.get(position).getUrl());
                    startActivity(shareIntent);
                    adapter.notifyDataSetChanged();
                }
                if (direction == ItemTouchHelper.LEFT) {
                    newsDatabase.addArticle(newsResponse.get(position));
                    TextToSpeechFeature.speakText(newsResponse.get(position).getTitle(), 0);
                    TextToSpeechFeature.speakText(newsResponse.get(position).getDescription(), 1);
                    adapter.notifyDataSetChanged();
//                    builder.setContentTitle("News Saved");
//                    builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.news));
//                    builder.setSmallIcon(R.drawable.save);
//                    builder.setContentText("The news article has been saved.");
//                    builder.setAutoCancel(true);
//                    Notification notification = builder.build();
//                    manager.notify(i++, notification);
                }
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(headlines_recycler_view);
        return view;
    }

    private void callApi() {
        NewsServiceBuilder.provideNewsService().getNews(sourceKey).enqueue(new NewsCallback<News>() {
            @Override
            public void onSuccess(News response) {
                if (dialog.isShowing())
                    dialog.cancel();
                newsResponse = response.getArticles();
                if(newsResponse.size()<=0)
                {
                    swipeRefreshLayout.setVisibility(View.GONE);
                    retryImage.setVisibility(View.VISIBLE);
                    retryText.setVisibility(View.VISIBLE);
                } else {
                    adapter = new NewsAdapter(newsResponse, NewsFragment.this);
                    headlines_recycler_view.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onError(ApiError error) {
                swipeRefreshLayout.setVisibility(View.GONE);
                retryImage.setVisibility(View.VISIBLE);
                retryText.setVisibility(View.VISIBLE);
                dialog.cancel();
                Log.d("manu", ""+error.message);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        String json = gson.toJson(newsResponse, type);
        outState.putString(RESPONSE_KEY, json);
        outState.putString(TITLE_KEY, title);
    }

    @Override
    public void onPause() {
        super.onPause();
        TextToSpeechFeature.stopSpeech();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        TextToSpeechFeature.stopSpeech();
    }

    @Override
    public void onNewsItemClicked(String url) {
        if (listener != null)
            listener.onNewsItemClick(url);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof NewsFragmentListener) {
            listener = (NewsFragmentListener) context;
        } else {
            throw new RuntimeException("Please implement NewsFragmentListener interface in your activity " +
                    "if you want to use News fragment");
        }
    }

    public interface NewsFragmentListener {
        void setToolbarTitle(String title);
        void onNewsItemClick(String url);
        void setFragment(Fragment fragment);
    }
}
