package manu.newschef.fragments;

import android.content.Context;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import manu.newschef.R;
import manu.newschef.application.MyApplication;
import manu.newschef.models.Article;
import manu.newschef.models.NewsAdapter;
import manu.newschef.services.NewsDatabase;
import manu.newschef.utils.TextToSpeechFeature;
import manu.newschef.views.NewsViewHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by imman on 11/5/2017.
 */

public class SavedNewsFragment extends Fragment implements NewsViewHolder.NewsClickListener {

    private List<Article> savedNews = new ArrayList<>();
    @BindView(R.id.swipe_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.headlines_recycler_view)
    RecyclerView headlines_recycler_view;
    private NewsDatabase newsDatabase = new NewsDatabase();
    private NewsAdapter adapter;
    private SavedFragmentListener listener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler_view, container, false);
        ButterKnife.bind(this, view);
        if(listener!=null) {
            listener.setFragment(this);
            listener.setToolbarTitle("Downloads");
        }
        swipeRefreshLayout.setEnabled(false);
        swipeRefreshLayout.setRefreshing(false);
        headlines_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity()));
        savedNews = newsDatabase.getArticles();
        adapter = new NewsAdapter(savedNews, SavedNewsFragment.this);
        headlines_recycler_view.setAdapter(adapter);

        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT  | ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                if (direction == ItemTouchHelper.RIGHT) {
                    newsDatabase.deleteArticle(savedNews.get(position));
                    adapter.list.remove(position);
                    adapter.notifyItemRemoved(position);
                    adapter.notifyDataSetChanged();
                }
                if (direction == ItemTouchHelper.LEFT) {
                    TextToSpeechFeature.speakText(savedNews.get(position).getTitle(), 0);
                    TextToSpeechFeature.speakText(savedNews.get(position).getDescription(), 1);
                    adapter.notifyDataSetChanged();
//                    newsDatabase.addArticle(newsResponse.get(position));
//                    builder.setContentTitle("News Saved");
//                    builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.news));
//                    builder.setSmallIcon(R.drawable.save);
//                    builder.setContentText("The news article has been saved.");
//                    builder.setAutoCancel(true);
//                    Notification notification = builder.build();
//                    manager.notify(i++, notification);
                }
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(headlines_recycler_view);
        return view;
    }

    @Override
    public void onNewsItemClicked(String url) {
        if (listener != null)
            listener.onNewsItemClick(url);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof SavedFragmentListener) {
            listener = (SavedFragmentListener) context;
        } else {
            throw new RuntimeException("Please implement SavedFragmentListener interface in your activity " +
                    "if you want to use SavedNews fragment");
        }
    }

    public interface SavedFragmentListener {
        void setFragment(Fragment fragment);
        void onNewsItemClick(String url);
        void setToolbarTitle(String title);
    }

    @Override
    public void onPause() {
        super.onPause();
        TextToSpeechFeature.stopSpeech();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        TextToSpeechFeature.stopSpeech();
    }
}
















