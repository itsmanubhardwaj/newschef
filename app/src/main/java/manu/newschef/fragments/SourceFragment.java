package manu.newschef.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import manu.newschef.R;
import manu.newschef.models.Source;
import manu.newschef.models.SourceAdapter;
import manu.newschef.views.SourceViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SourceFragment extends Fragment implements SourceViewHolder.SourceClickListener {

    @BindView(R.id.headlines_recycler_view)
    RecyclerView sourceRecyclerView;
    @BindView(R.id.swipe_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    private SourceFragmentListener listener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler_view, container, false);
        ButterKnife.bind(this, view);
        if (listener != null) {
            listener.setFragment(this);
            listener.setToolbarTitle("Home");
        }
        swipeRefreshLayout.setEnabled(false);
        swipeRefreshLayout.setRefreshing(false);
        sourceRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        populateList();
        return view;
    }

    public void populateList() {
        List<Source> sourceList = new ArrayList<>();
        sourceList.add(new Source("ABC News", "abc-news",
                "https://besticon-demo.herokuapp.com/icon?url=http://abcnews.go.com&size=70..120..200", "General", "United States"));
        sourceList.add(new Source("Al Jazeera", "al-jazeera-english",
                "https://besticon-demo.herokuapp.com/icon?url=http://www.aljazeera.com&size=70..120..200", "General", "United States"));
        sourceList.add(new Source("Ars Technica", "ars-technica",
                "https://besticon-demo.herokuapp.com/icon?url=http://arstechnica.com&size=70..120..200", "Technology", "United States"));
        sourceList.add(new Source("Axios", "axios",
                "https://besticon-demo.herokuapp.com/icon?url=https://www.axios.com&size=70..120..200", "General", "United States"));
        sourceList.add(new Source("Breitbart News", "breitbart-news",
                "https://besticon-demo.herokuapp.com/icon?url=http://www.breitbart.com&size=70..120..200", "General", "United States"));
        sourceList.add(new Source("Business Insider", "business-insider",
                "https://besticon-demo.herokuapp.com/icon?url=http://www.businessinsider.com&size=70..120..200", "Business", "United States"));
        sourceList.add(new Source("Buzzfeed", "buzzfeed",
                "https://besticon-demo.herokuapp.com/icon?url=https://www.buzzfeed.com&size=70..120..200", "Entertainment", "United States"));
        sourceList.add(new Source("CBC News", "cbc-news",
                "https://besticon-demo.herokuapp.com/icon?url=http://www.cbc.ca/news&size=70..120..200", "General", "Canada"));
        sourceList.add(new Source("CBS News", "cbs-news",
                "https://besticon-demo.herokuapp.com/icon?url=http://www.cbsnews.com&size=70..120..200", "General", "United States"));
        sourceList.add(new Source("CNN", "cnn",
                "https://besticon-demo.herokuapp.com/icon?url=http://us.cnn.com&size=70..120..200", "General", "United States"));
        sourceList.add(new Source("Daily Mail", "daily-mail",
                "https://besticon-demo.herokuapp.com/icon?url=http://www.dailymail.co.uk/home/index.html&size=70..120..200", "Entertainment", "United Kingdom"));
        sourceList.add(new Source("Engadget", "engadget",
                "https://besticon-demo.herokuapp.com/icon?url=https://www.engadget.com&size=70..120..200", "Technology", "United States"));
        sourceList.add(new Source("Entertainment Weekly", "entertainment-weekly",
                "https://besticon-demo.herokuapp.com/icon?url=http://www.ew.com&size=70..120..200", "Entertainment", "United States"));
        sourceList.add(new Source("Espn", "espn",
                "https://besticon-demo.herokuapp.com/icon?url=http://espn.go.com&size=70..120..200", "Sports", "United States"));
        sourceList.add(new Source("ESPN Cric Info", "espn-cric-info",
                "https://besticon-demo.herokuapp.com/icon?url=http://www.espncricinfo.com/&size=70..120..200", "Sports", "United States"));
        sourceList.add(new Source("Financial Post", "financial-post",
                "https://besticon-demo.herokuapp.com/icon?url=http://business.financialpost.com&size=70..120..200", "Business", "Canada"));
        sourceList.add(new Source("Financial Times", "financial-times",
                "https://besticon-demo.herokuapp.com/icon?url=http://www.ft.com/home/uk&size=70..120..200", "Business", "United Kingdom"));
        sourceList.add(new Source("Fortune", "fortune",
                "https://besticon-demo.herokuapp.com/icon?url=http://fortune.com&size=70..120..200", "Business", "United States"));
        sourceList.add(new Source("Fox Sports", "fox-sports",
                "https://besticon-demo.herokuapp.com/icon?url=http://www.foxsports.com&size=70..120..200", "Sports", "United States"));
        sourceList.add(new Source("Metro", "metro",
                "https://besticon-demo.herokuapp.com/icon?url=http://metro.co.uk&size=70..120..200", "General", "United Kingdom"));
        sourceList.add(new Source("MTV News", "mtv-news",
                "https://besticon-demo.herokuapp.com/icon?url=http://www.mtv.com/news&size=70..120..200", "Entertainment", "United States"));
        sourceList.add(new Source("National Geographic", "national-geographic",
                "https://besticon-demo.herokuapp.com/icon?url=http://news.nationalgeographic.com&size=70..120..200", "Science", "United States"));
        sourceList.add(new Source("NFL News", "nfl-news",
                "https://besticon-demo.herokuapp.com/icon?url=http://www.nfl.com/news&size=70..120..200", "Sports", "United States"));
        sourceList.add(new Source("Polygon", "polygon",
                "https://besticon-demo.herokuapp.com/icon?url=http://www.polygon.com&size=70..120..200", "Entertainment", "United States"));
        sourceList.add(new Source("Recode", "recode",
                "https://besticon-demo.herokuapp.com/icon?url=http://www.recode.net&size=70..120..200", "Technology", "United States"));
        sourceList.add(new Source("TechCrunch", "techcrunch",
                "https://besticon-demo.herokuapp.com/icon?url=https://techcrunch.com&size=70..120..200", "Technology", "United States"));
        sourceList.add(new Source("TechRadar", "techradar",
                "https://besticon-demo.herokuapp.com/icon?url=http://www.techradar.com&size=70..120..200", "Technology", "United States"));
        sourceList.add(new Source("The Guardian", "the-guardian-au",
                "https://besticon-demo.herokuapp.com/icon?url=https://www.theguardian.com/au&size=70..120..200", "General", "United Kingdom"));
        sourceList.add(new Source("The Hindu", "the-hindu",
                "https://besticon-demo.herokuapp.com/icon?url=http://www.thehindu.com&size=70..120..200", "General", "India"));
        sourceList.add(new Source("The New York Times", "the-new-york-times",
                "https://besticon-demo.herokuapp.com/icon?url=http://www.nytimes.com&size=70..120..200", "General", "United States"));
        sourceList.add(new Source("The Telegraph", "the-telegraph",
                "https://besticon-demo.herokuapp.com/icon?url=http://www.telegraph.co.uk&size=70..120..200", "General", "United Kingdom"));
        sourceList.add(new Source("The Verge", "the-verge",
                "https://besticon-demo.herokuapp.com/icon?url=http://www.theverge.com&size=70..120..200", "Technology", "United States"));
        sourceList.add(new Source("The Washington Post", "the-washington-post",
                "https://besticon-demo.herokuapp.com/icon?url=https://www.washingtonpost.com&size=70..120..200", "General", "United States"));
        sourceList.add(new Source("Wired", "wired",
                "https://besticon-demo.herokuapp.com/icon?url=https://www.wired.com&size=70..120..200", "Technology", "United States"));
        sourceRecyclerView.setAdapter(new SourceAdapter(sourceList, SourceFragment.this));
    }

    @Override
    public void onSourceClicked(Source source) {
        if (listener != null) {
            listener.onSourceItemClick(source);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof SourceFragmentListener) {
            listener = (SourceFragmentListener) context;
        } else {
            throw new RuntimeException("Please implement SourceFragmentListener interface in your activity " +
                    "if you want to use Source fragment");
        }
    }

    public interface SourceFragmentListener {
        void setToolbarTitle(String title);

        void onSourceItemClick(Source source);

        void setFragment(Fragment fragment);
    }
}
