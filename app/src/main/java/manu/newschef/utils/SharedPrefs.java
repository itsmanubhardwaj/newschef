package manu.newschef.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Set;

import manu.newschef.application.MyApplication;

/**
 * Created by imman on 3/25/2018.
 */

public class SharedPrefs {

    private static String CHECKED_COUNTRIES = "checked_countries";
    private static String CHECKED_CATEGORIES = "checked_categories";
    private static String BUTTON_VALUE = "false";

    private static SharedPreferences getPrefrences() {
        Context c = MyApplication.getAppContext();
        return c.getSharedPreferences("main", Context.MODE_PRIVATE);
    }

    public static void setButton(Boolean value) {
        getPrefrences().edit().putBoolean(BUTTON_VALUE, value).commit();
    }

    public static Boolean getButton() {
        return getPrefrences().getBoolean(BUTTON_VALUE, false);
    }

    public static void setCheckedCountryIds(Set<String> ids) {
        getPrefrences().edit().putStringSet(CHECKED_COUNTRIES, ids).commit();
    }

    public static Set<String> getCheckedCountryIds() {
        return getPrefrences().getStringSet(CHECKED_COUNTRIES, null);
    }

    public static void setCheckedCategoryIds(Set<String> ids) {
        getPrefrences().edit().putStringSet(CHECKED_CATEGORIES, ids).commit();
    }

    public static Set<String> getCheckedCategoryIds() {
        return getPrefrences().getStringSet(CHECKED_CATEGORIES, null);
    }

}
