package manu.newschef.utils;

import android.databinding.ObservableField;
import android.support.annotation.NonNull;


import io.reactivex.Observable;
import static android.databinding.Observable.OnPropertyChangedCallback;

/**
 * Created by imman on 2/20/2018.
 */

public class RxUtils {

    private RxUtils() {
    }

    public static <T> Observable<T> toObservable(@NonNull final ObservableField<T> observableField) {

        return Observable.create(e -> observableField.addOnPropertyChangedCallback(new OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(android.databinding.Observable observable, int i) {
                if (observable == observableField) {
                    e.onNext(observableField.get());
                }
            }
        }));
    }
}
