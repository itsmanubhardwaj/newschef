package manu.newschef.utils;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import manu.newschef.application.MyApplication;


/**
 * Created by imman on 10/15/2017.
 */

public class ImageBindingAdapter {
    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(ImageView imageView, String url) {
        if (url != null) {
            Picasso.with(MyApplication.getAppContext()).load(url).fit().into(imageView);
        }
    }
}
