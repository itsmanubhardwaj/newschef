package manu.newschef.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import manu.newschef.application.MyApplication;
import manu.newschef.models.ApiError;
import manu.newschef.models.Article;
import manu.newschef.models.NewsCallback;
import manu.newschef.response.News;
import manu.newschef.services.NewsServiceBuilder;

/**
 * Created by imman on 4/17/2018.
 */

public class NewsBroadcastReceiver extends BroadcastReceiver {

    private List<Integer> countryList;
    private List<Integer> categoryList;
    private Random random;

    @Override
    public void onReceive(Context context, Intent intent) {
        countryList = new ArrayList<>();
        categoryList = new ArrayList<>();
        random = new Random();
        sendRequest();
    }

    private void sendRequest() {
        if (MyApplication.batteryLevel() < 15) {
            return;
        }
        if (SharedPrefs.getCheckedCountryIds() != null) {
            for (String s : SharedPrefs.getCheckedCountryIds()) {
                countryList.add(Integer.valueOf(s));
            }
        }
        if (SharedPrefs.getCheckedCategoryIds() != null) {
            for (String s : SharedPrefs.getCheckedCategoryIds()) {
                categoryList.add(Integer.valueOf(s));
            }
        }
        String category = "general";
        String country = "in";
        int countryInt = countryList.size();
        int categoryInt = categoryList.size();
        if (countryInt > 0) {
            int countryPosition = random.nextInt(countryInt);
            country = MyApplication.getAppContext().getResources().getResourceEntryName(countryList.get(countryPosition));
        }
        if (categoryInt > 0) {
            int categoryPosition = random.nextInt(categoryInt);
            category = MyApplication.getAppContext().getResources().getResourceEntryName(categoryList.get(categoryPosition));
        }
        callApi(country, category);
    }

    public void callApi(String country, String category) {
        NewsServiceBuilder.provideNewsService().getNewsUpdates(country, category).enqueue(new NewsCallback<News>() {
            @Override
            public void onSuccess(News response) {
                Article article = response.getArticles().get(0);
                new SendNotification(0).execute(article);
            }

            @Override
            public void onError(ApiError error) {
            }
        });
    }
}
