package manu.newschef.utils;

import android.speech.tts.TextToSpeech;

import java.util.Locale;

import manu.newschef.application.MyApplication;

/**
 * Created by imman on 3/15/2018.
 */

public class TextToSpeechFeature {

    private static TextToSpeech textToSpeech;

    public static void init() {
        textToSpeech = new android.speech.tts.TextToSpeech(MyApplication.getAppContext(), status -> {
            if (status != android.speech.tts.TextToSpeech.ERROR) {
                textToSpeech.setLanguage(Locale.US);
            }
        });
    }

    public static void speakText(String text, int type) {
        if(type == 0) {
            textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        } else if(type == 1) {
            textToSpeech.speak(text, TextToSpeech.QUEUE_ADD, null);
        }
    }

    public static void stopSpeech() {
        textToSpeech.stop();
    }
}
