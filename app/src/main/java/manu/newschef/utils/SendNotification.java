package manu.newschef.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import manu.newschef.R;
import manu.newschef.activities.MainActivity;
import manu.newschef.application.MyApplication;
import manu.newschef.models.Article;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by imman on 3/24/2018.
 */

public class SendNotification extends AsyncTask<Article, Void, Bitmap> {

    private Article article;
    private int notificationId;

    public SendNotification(int i) {
        notificationId = i;
    }

    @Override
    protected Bitmap doInBackground(Article... article) {

        InputStream in;
        this.article = article[0];
        try {

            URL url = new URL(this.article.getUrlToImage());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            in = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(in);
            return myBitmap;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap result) {

        super.onPostExecute(result);
        try {
            NotificationCompat.Builder builder;
            NotificationManager manager;
            builder = new NotificationCompat.Builder(MyApplication.getAppContext());
            manager = (NotificationManager) MyApplication.getAppContext().getSystemService(NOTIFICATION_SERVICE);
            builder.setContentText(article.getTitle());
            String titleName = article.getSource().getName();
            String string = titleName.substring(0, titleName.indexOf(".")).replace("WWW.", "")
                    .replace("wwww.", "").replace("-", " ");
            builder.setContentTitle("New Story by " + string);
            builder.setSmallIcon(R.drawable.logo);
            Intent intent = new Intent(MyApplication.getAppContext(), MainActivity.class);
            intent.putExtra("title_name", titleName);
            intent.putExtra("url", article.getUrl());
            intent.putExtra("fragment_name", "ArticleFragment");
            PendingIntent pending = PendingIntent.getActivity(MyApplication.getAppContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT);
            builder.setContentIntent(pending);
            builder.setWhen(System.currentTimeMillis());
            builder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
            builder.setVibrate(new long[]{1000, 1000});
            builder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(result));
            builder.setAutoCancel(true);
            Notification notification = builder.build();
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            manager.notify(notificationId, notification);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
