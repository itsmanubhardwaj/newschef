package manu.newschef.models;

import manu.newschef.response.News;
import manu.newschef.response.SearchResponse;

import manu.newschef.response.SourceResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by imman on 8/16/2017.
 */

public interface NewsInterface {
        @GET("/v2/top-headlines")
        Call<News> getNews(@Query("sources") String source);

        @GET("/v2/top-headlines")
        Call<News> getNewsUpdates(@Query("country") String country, @Query("category") String category);

        @GET("/v2/everything")
        Call<SearchResponse> getSearchResults(@Query("q") String keyword, @Query("sortBy") String sortBy);
}
