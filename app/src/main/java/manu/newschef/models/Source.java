package manu.newschef.models;

/**
 * Created by imman on 10/6/2017.
 */

public class Source {

    public String sourceName;
    public String sourceIcon;
    public String country;
    public String category;
    public String sourceKey;

    public Source(String sourceName, String sourceKey, String icon, String category, String country) {
        this.sourceName = sourceName;
        this.sourceKey = sourceKey;
        sourceIcon = icon;
        this.category = category;
        this.country = country;
    }
}