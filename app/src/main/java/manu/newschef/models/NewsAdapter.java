package manu.newschef.models;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import manu.newschef.views.NewsViewHolder;
import manu.newschef.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by imman on 8/16/2017.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsViewHolder> {

    public List<Article> list = new ArrayList<>();
    private Fragment fragment;

    public NewsAdapter(List<Article> list, Fragment fragment) {
        this.list = list;
        this.fragment = fragment;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_row_headlines, parent, false);
        return new NewsViewHolder(v, fragment);
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        Article obj = list.get(position);
        holder.bind(obj);
    }

    @Override
    public int getItemCount() {
        if (list.size() != 0) {
            return list.size();
        }
        else return 0;
    }
}
