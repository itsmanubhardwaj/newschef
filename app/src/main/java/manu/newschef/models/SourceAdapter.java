package manu.newschef.models;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import manu.newschef.R;
import manu.newschef.views.SourceViewHolder;

import java.util.ArrayList;
import java.util.List;

public class SourceAdapter extends RecyclerView.Adapter<SourceViewHolder> {

    private List<Source> sourceList = new ArrayList<>();
    private Fragment fragment;

    public SourceAdapter(List<Source> sourceList, Fragment fragment) {
        this.sourceList = sourceList;
        this.fragment = fragment;
    }

    @Override
    public SourceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_source_row, parent, false);
        return new SourceViewHolder(view, fragment);
    }

    @Override
    public void onBindViewHolder(SourceViewHolder holder, int position) {
        Source obj = sourceList.get(position);
        holder.bind(obj);
    }

    @Override
    public int getItemCount() {
        return sourceList.size();
    }
}
