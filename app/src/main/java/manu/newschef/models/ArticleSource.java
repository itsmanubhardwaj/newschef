package manu.newschef.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by imman on 3/25/2018.
 */

public class ArticleSource {


    @SerializedName("id")
    @Expose
    private String sourceId;
    @SerializedName("name")
    @Expose
    private String name;

    public ArticleSource(String sourceId, String name) {
        this.sourceId = sourceId;
        this.name = name;
    }

    public String getSourceId() {
        return sourceId;
    }

    public String getName() {
        return name;
    }
}
