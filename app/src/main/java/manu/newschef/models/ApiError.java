package manu.newschef.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by imman on 10/6/2017.
 */

public class ApiError {

    @Expose
    @SerializedName("message")
    public String message;

    @Expose
    @SerializedName("code")
    public String code;

    @Expose
    @SerializedName("status")
    public String status;

    public ApiError(String message, String code) {
        this.message = message;
        this.code = code;
    }
}
