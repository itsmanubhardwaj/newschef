package manu.newschef.models;

import android.database.Cursor;
import android.database.CursorWrapper;

/**
 * Created by imman on 9/12/2017.
 */

public class ArticleCursor extends CursorWrapper {

    public ArticleCursor(Cursor cursor) {
        super(cursor);
    }

    public Article getArticles() {
        return new Article(getString(this.getColumnIndex("Title")),getString(this.getColumnIndex("Description")), getString(this.getColumnIndex("Url")));
    }
}
