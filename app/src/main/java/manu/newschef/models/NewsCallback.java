package manu.newschef.models;

import android.support.annotation.NonNull;

import manu.newschef.models.ApiError;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class NewsCallback<T> implements Callback<T> {

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        if (response.isSuccessful()) {
            onSuccess(response.body());
        } else {
            Gson gson = new Gson();
            try {
                ApiError error = gson.fromJson(response.errorBody().string(), ApiError.class);
                onError(error);
            } catch (IOException e) {
                onError(new ApiError("Unknown error", "Error"));
            }
        }
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        onError(new ApiError(t.getMessage(), "Error"));
    }

    public abstract void onSuccess(T response);

    public abstract void onError(ApiError error);
}
