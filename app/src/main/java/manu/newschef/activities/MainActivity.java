package manu.newschef.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import manu.newschef.R;
import manu.newschef.fragments.ArticleFragment;
import manu.newschef.fragments.NewsFragment;
import manu.newschef.fragments.SavedNewsFragment;
import manu.newschef.fragments.SearchFragment;
import manu.newschef.fragments.SettingsFragment;
import manu.newschef.fragments.SourceFragment;
import manu.newschef.fragments.TrendingFragment;
import manu.newschef.models.Source;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SourceFragment.SourceFragmentListener,
        NewsFragment.NewsFragmentListener, ArticleFragment.ArticleFragmentListener,
        SearchFragment.SearchFragmentListener, TrendingFragment.TrendingFragmentListener,
        SavedNewsFragment.SavedFragmentListener, SettingsFragment.SettingsFragmentListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fragment_container)
    FrameLayout frameLayout;
    private FragmentManager fragmentManager;
    private TextView actionBarTitle;
    private boolean doubleBackToExitPressedOnce = false;
    private Fragment fragment;
    private static String FRAGMENT_KEY = "fragment";
    private String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        fragmentManager = getSupportFragmentManager();
        Intent intent = getIntent();
        String titleName = intent.getStringExtra("title_name");
        String fragmentName = intent.getStringExtra("fragment_name");
        String urlToArticle = intent.getStringExtra("url");
        try {
            if (fragmentName!=null && fragmentName.equals("ArticleFragment")) {
                String string = titleName.substring(0, titleName.indexOf(".")).replace("WWW.", "")
                        .replace("wwww.", "").replace("-", " ");
                fragment = ArticleFragment.createInstance(urlToArticle, string);
                fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit();
            } else {
                if (savedInstanceState != null) {
                    fragment = fragmentManager.getFragment(savedInstanceState, FRAGMENT_KEY);
                } else {
                    toolbar.setTitle("Home");
                    fragment = new SourceFragment();
                    fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        setSupportActionBar(toolbar);
        actionBarTitle = toolbar.findViewById(R.id.title);
        ImageView search = toolbar.findViewById(R.id.search);
        search.setOnClickListener(view -> {
            fragment = new SearchFragment();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment)
                    .addToBackStack("demo").commit();
        });
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        if (fragment instanceof SourceFragment) {
            doubleBackToExitPressedOnce = true;
            Snackbar.make(frameLayout, "Press back again to exit", Snackbar.LENGTH_SHORT).show();
            fragmentManager.popBackStackImmediate();

            new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.home) {
            fragment = new SourceFragment();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment)
                    .commit();

        } else if (id == R.id.trending) {
            fragment = new TrendingFragment();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment)
                    .addToBackStack("demo").commit();

        } else if (id == R.id.downloads) {

            fragmentManager.beginTransaction().replace(R.id.fragment_container, new SavedNewsFragment())
                    .addToBackStack("demo").commit();
            toolbar.setTitle("Downloads");
        } else if (id == R.id.settings) {

            fragmentManager.beginTransaction().replace(R.id.fragment_container, new SettingsFragment())
                    .addToBackStack("demo").commit();
            toolbar.setTitle("Settings");
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void setToolbarTitle(String title) {
        if (title != null) {
            actionBarTitle.setText(title);
        }
    }

    @Override
    public void onSourceItemClick(Source source) {
        fragment = NewsFragment.createInstance(source);
        title = source.sourceName;
        fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment)
                .addToBackStack("demo").commit();
    }

    @Override
    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    @Override
    public void onNewsItemClick(String url) {
        fragment = ArticleFragment.createInstance(url, title);
        fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment)
                .addToBackStack("demo").commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        fragmentManager.putFragment(outState, FRAGMENT_KEY, fragment);
    }
}
