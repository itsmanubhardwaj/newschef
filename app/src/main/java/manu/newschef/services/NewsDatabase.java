package manu.newschef.services;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import manu.newschef.application.MyApplication;
import manu.newschef.models.Article;
import manu.newschef.models.ArticleCursor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by imman on 9/12/2017.
 */

public class NewsDatabase extends SQLiteOpenHelper {

    private String TABLE_CONTACTS = "manu";
    private String KEY_ID = "Title";

    public NewsDatabase() {
        super(MyApplication.getAppContext(), "manu", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE `manu` (\n" +
                "\t`Id`\tINTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`Title`\tTEXT UNIQUE,\n" +
                "\t`Description`\tTEXT UNIQUE,\n" +
                "\t`Url`\tTEXT\n" +
                ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        onCreate(sqLiteDatabase);
    }

    public void addArticle(Article article) {
        SQLiteDatabase db = new NewsDatabase().getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("Title", article.getTitle());
        values.put("Description", article.getDescription());
        values.put("Url", "C:\\\\Users\\\\imman\\\\AndroidStudioProjects\\\\NewsChef\\\\app\\\\src\\\\main\\\\res\\\\mipmap-mdpi\\\\ic_launcher.png");
        db.insert(TABLE_CONTACTS, null, values);
        db.close();
    }

    public List<Article> getArticles() {
        List<Article> articles = new ArrayList<>();
        SQLiteDatabase db = new NewsDatabase().getReadableDatabase();
        Cursor cursor = db.query(TABLE_CONTACTS, null, null, null, null, null, null);
        ArticleCursor articleCursor = new ArticleCursor(cursor);
        while (articleCursor.moveToNext()) {
            Article result = articleCursor.getArticles();
            articles.add(result);
        }
        return articles;
    }

    public void deleteArticle(Article article) {
        SQLiteDatabase db = new NewsDatabase().getWritableDatabase();
        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
                new String[]{String.valueOf(article.getTitle())});
        db.close();
    }
}
