package manu.newschef.views;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import manu.newschef.BR;
import manu.newschef.models.Article;

public class NewsViewHolder extends RecyclerView.ViewHolder {

    private ViewDataBinding binding;
    private String url;
    private NewsClickListener listener;


    public NewsViewHolder(View itemView, Fragment fragment) {
        super(itemView);
        listener = (NewsClickListener) fragment;
        binding = DataBindingUtil.bind(itemView);
        itemView.setOnClickListener(v -> {
            if (listener != null)
                listener.onNewsItemClicked(url);
        });
    }

    public void bind(Article obj) {
        binding.setVariable(BR.item, obj);
        binding.executePendingBindings();
        url = obj.getUrl();
    }

    public interface NewsClickListener {
        void onNewsItemClicked(String url);
    }
}

