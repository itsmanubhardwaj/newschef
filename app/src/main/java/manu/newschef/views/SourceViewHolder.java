package manu.newschef.views;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.media.Image;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import manu.newschef.BR;
import manu.newschef.R;
import manu.newschef.models.Source;

public class SourceViewHolder extends RecyclerView.ViewHolder {

    private ViewDataBinding binding;
    private Source source;
    private SourceClickListener listener;

    public SourceViewHolder(View itemView, Fragment fragment) {
        super(itemView);
        listener = (SourceClickListener) fragment;
        binding = DataBindingUtil.bind(itemView);
        itemView.setOnClickListener(v -> {
            if (listener != null)
                listener.onSourceClicked(source);
        });
    }

    public void bind(Source obj) {
        binding.setVariable(BR.source, obj);
        source = obj;
        binding.executePendingBindings();
    }

    public interface SourceClickListener {
        void onSourceClicked(Source source);
    }
}
