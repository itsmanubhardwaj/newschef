package manu.newschef.application;

import android.app.AlarmManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import manu.newschef.utils.TextToSpeechFeature;

public class MyApplication extends Application {

    private static Context context;
    private static final long IMAGE_CACHE_SIZE = 1024 * 1024 * 150;
    private static AlarmManager alarmManager;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        TextToSpeechFeature.init();
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        setupPicassoConfig();
    }

    public static AlarmManager getAlarmManager() {
        return alarmManager;
    }

    public static Context getAppContext() {
        return context;
    }

    public static void setupPicassoConfig() {
        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context, IMAGE_CACHE_SIZE));
        Picasso built = builder.build();
        Picasso.setSingletonInstance(built);
    }

    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) MyApplication.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public static int batteryLevel() {
        Intent intent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
        int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 100);
        return (level * 100) / scale;
    }
}
